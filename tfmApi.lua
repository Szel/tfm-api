return {
    -- events
    eventChatCommand = {
        type = "function",
        description = "This event triggers when the player types a message beginning with « ! » in the chat.",
        args = "playerName: string, message: string"
    },

    eventEmotePlayed = {
        type = "function",
        description = "This event triggers when the player does an emote.",
        args = "playerName: string, emoteId: int"
    },

    eventFileLoaded = {
        type = "function",
        description = "This event triggers when a file is loaded.",
        args = "fileName: string, file: string"
    },

    eventFileSaved = {
        type = "function",
        description = "This event triggers when a file is saved.",
        args = "fileName: string"
    },

    eventKeyboard = {
        type = "function",
        description = "This event triggers when a player presses a key.",
        args = "playerName: string, keyCode: int, down: bool, xPlayerPostition: int, yPlayerPosition: int"
    },

    eventMouse = {
        type = "function",
        description = "This event triggers when a player click his mouse.",
        args = "playerName: string, xMousePosition: int, yMousePosition: int"
    },

    eventLoop = {
        type = "function",
        description = "This event occurs every 500 miliseconds.",
        args = "currentTime: int, timeRemaining: int"
    },

    eventNewGame = {
        type = "function",
        description = "This event triggers at the start of a new game.",
    },

    eventNewPlayer = {
        type = "function",
        description = "This event triggers when a new player joins the room.",
        args = "playerName: string"
    },

    eventPlayerDataLoaded = {
        type = "function",
        description = "This event triggers when the data is loaded.",
        args = "playerName: string, data: string"
    },

    eventPlayerDied = {
        type = "function",
        description = "This event triggers when a player dies.",
        args = "playerName: string"
    },

    eventPlayerGetCheese = {
        type = "function",
        description = "This event triggers when a player gets the cheese.",
        args = "playerName: string"
    },

    eventPlayerLeft = {
        type = "function",
        description = "This event triggers when a player leaves the room.",
        args = "playerName: string"
    },

    eventPlayerVampire = {
        type = "function",
        description = "This event triggers when a player transforms into a vampire.",
        args = "playerName: string"
    },

    eventPlayerWon = {
        type = "function",
        description = "This event triggers when a player wins the round.",
        args = "playerName: string, timeElapsed: int, timeElapsedSinceRespawn: int"
    },

    eventPlayerRespawn = {
        type = "function",
        description = "This event triggers when a player respawn.",
        args = "playerName: string"
    },

    eventPopupAnswer = {
        type = "function",
        description = "This event triggers when a player answer to a popup.",
        args = "popupId: int, playerName: string, answer: string"
    },

    eventSummoningStart = {
        type = "function",
        description = "This event triggers when a player starts an invocation.",
        args = "playerName: string, objectType: int, xPosition: int, yPosition: int, angle: int"
    },

    eventSummoningCancel = {
        type = "function",
        description = "This event triggers when a player cancels an invocation.",
        args = "playerName: string"
    },

    eventSummoningEnd = {
        type = "function",
        description = "This event triggers when a player has finished an invocation.",
        args = "playerName: string, objectType: int, xPosition: int, yPosition: int, angle: int, xSpeed: int, ySpeed: int, other: table"
    },

    eventTextAreaCallback = {
        type = "function",
        description = "This event triggers when a player clicks on a flash text event. Example : <a href="event:callbackString">Click here</a>. If the string callback begins with '#clear,' the text area is cleared.",
        args = "textAreaId: int, playerName: string, callback: string"
    },

    
    --
    tfm = {
        type = "lib",
        description = "Transformice library",
        childs = {
        exec = {
            type = "lib",
            description = "Transformice functions",

            childs = {
                addShamanObject = { 
                    type = "function", 
                    description = "Adds a Shaman object into the game", 
                    args = "objectId: int, xPosition: int, yPosition: int, angle: int, xSpeed: int, ySpeed: int, ghost: bool" 
                },
                snow = {
                    type = "function",
                    description = "Makes the snow fall.",
                    args = "[seconds: int [, snowballPower int]]"
                },            
                disableAutoNewGame = { 
                    type = "function", 
                    description = "Deactivates the automatic renewal of rounds.", 
                    args = "yes: bool" 
                },
                setShaman = { 
                    type = "function", 
                    description = "Set a shaman.", 
                    args = "playerName: string" 
                },
                addImage = { 
                    type = "function", 
                    description = [[Add an image to the map. Target can be :
                        #mobileId
                        $playerName
                        %playerName
                        ?backgroundLayerDepth
                        _groundLayerDepth
                        !foregroundLayerDepth
                        &fixedLayerDepth
                        If targetPlayer is NIL, it sends it to all players.
                        ]], 
                    args = "imageName: string, target: string, xPosition: int, yPosition: int, targetPlayer: string",
                    returns = "imageId: int"
                },
                removeJoint = { 
                    type = "function", 
                    description = "Removes a joint from the game.", 
                    args = "id: int" 
                },
                respawnPlayer = { 
                    type = "function", 
                    description = "Respawn a player.", 
                    args = "playerName: string" 
                },
                newGame = {
                    type = "function",
                    description = "Launch a new game. Use : 6 (vanilla map), @42583 (editor map), #4 (perm category map), begin with '<' (xml map)", 
                    args = "mapCode: string" 
                },
                disableAllShamanSkills = {
                    type = "function",
                    description = "Desactivates all shaman skills.", 
                    args = "yes: bool" 
                },
                setGameTime = {
                    type = "function",
                    description = "Set the game time.", 
                    args = "seconds: int" 
                },
                movePlayer = {
                    type = "function",
                    description = "Defines the speed and position of a player.", 
                    args = "playerName: string, xPosition: int, yPosition: int, offset: bool, xSpeed: int, ySpeed: int, offset: bool" 
                },
                removeImage = {
                    type = "function",
                    description = "Remove an image.", 
                    args = "imageId: int" 
                },
                chatMessage = {
                    type = "function",
                    description = "Sends a message to all the players in the room. If playerName isn't NIL, the message is sent to this player only.", 
                    args = "message: string, playerName: string" 
                },
                setVampirePlayer = {
                    type = "function",
                    description = "Set player as vampire.", 
                    args = "playerName: string" 
                },
                explosion = {
                    type = "function",
                    description = "Throw an explosion.", 
                    args = "xPosition: int, yPosition: int, power: int, distance: int, miceOnly: bool" 
                },
                moveObject = {
                    type = "function",
                    description = "Defines the speed and position of an object.", 
                    args = "objectId: int, xPosition: int, yPosition: int, offset: bool, xSpeed: int, ySpeed: int, offset: bool" 
                },
                disableAutoScore = {
                    type = "function",
                    description = "Deactivates the automatic scoring management.", 
                    args = "yes: bool" 
                },
                giveCheese = {
                    type = "function",
                    description = "Gives the cheese to a player.", 
                    args = "playerName: string" 
                },
                addPhysicObject = {
                    type = "function",
                    description = [[Adds a physic object into the game.
                        bodyDef properties are the same as the map editor ones:
                        - type (Int), width (Int), height (Int), foreground (Boolean), friction (Float), restitution (Float), angle (Int), color (Int), miceCollision (Boolean), groundCollision (Boolean)
                        - dynamic (Boolean), fixedRotation (Boolean), mass (Int), linearDamping (Float), angularDamping (Float) for dynamic grounds
                        Note: On the map editor, players can also add a 'lua="id"' property in a ground definition in the XML code to be able to interact with it with LUA code.
                    ]], 
                    args = "id: int, xPosition: int, yPosition: int, bodyDef: table" 
                },
                giveMeep = {
                    type = "function",
                    description = "Gives the meep competence to a player.", 
                    args = "playerName: string" 
                },
                displayParticle = {
                    type = "function",
                    description = "Add a particle. If targetPlayer is NIL, it sends it to all players.", 
                    args = "particleId: int, xPosition: int, yPosition: y, xSpeed: float, ySpeed: float, xAcceleretion: float, yAcceleration: float, targetPlayer: string" 
                },
                removePhysicObject = {
                    type = "function",
                    description = "Removes a physic body from the game.", 
                    args = "objectId: int" 
                },
                disableAutoTimeLeft = {
                    type = "function",
                    description = "Deactivates the automatic time change.", 
                    args = "yes: bool" 
                },
                setPlayerScore = {
                    type = "function",
                    description = "Set the player's score.", 
                    args = "playerName: string, score: int, add: bool" 
                },
                killPlayer = {
                    type = "function",
                    description = "Kills the selected player.", 
                    args = "playerName: string" 
                },
                addJoint = {
                    type = "function",
                    description = [[Adds a joint between two physic objects.
                        jointDef properties are:
                        - type (Int): 0 -> distance joint, 1 -> prismatic joint, 2 -> pulley joint, 3 -> revolute joint
                        - point1 (String "x,y"): location of the ground1 anchor (default: the ground1's center)
                        - point2 (String "x,y"): location of the ground2 anchor (default: the ground2's center), only used with distance and pulley joints
                        - point3 (String "x,y"), point4 (String "x,y"): locations of the pulley's anchors, only used with pulley joints
                        - frequency (Float), damping (Float): distance joints' frequency and damping ratio
                        - axis (String "x,y"), angle (Int): prismatic joints' axis and angle
                        - limit1 (Float), limit2 (Float), forceMotor (Float), speedMotor (Float): prismatic and revolute joints' translation/rotation limits and motors
                        ratio (Float): revolute joints' ratio
                        - line (Int), color (Int), alpha (Float), foreground (Boolean): if none of these properties is defined, the joint won't be drawn
                        Note: On the map editor, players can also add a 'lua="id"' property in a joint definition in the XML code to be able to interact with it with LUA code.]], 
                    args = "id: int, physicObject1: int, physicObject2: int, jointDef: table" 
                },
                setRoomMaxPlayers = {
                    type = "function",
                    description = "Sets the max number of players in a room.", 
                    args = "maxPlayers: int" 
                },
                setNameColor = {
                    type = "function",
                    description = "Changes the player's name's color.", 
                    args = "playerName: string, color: int" 
                },
                disableAfkDeath = {
                    type = "function",
                    description = "Desactivates the automatic afk death.", 
                    args = "yes: bool" 
                },
                removeObject = {
                    type = "function",
                    description = "Remove a physical object.", 
                    args = "objectId: int" 
                },
                addConjuration = {
                    type = "function",
                    description = "Add conjuration to the map.", 
                    args = "xPosition: int, yPosition: int, timeInMillis: int" 
                },
                setUIMapName = {
                    type = "function",
                    description = "Set text map name.", 
                    args = "text: string" 
                },
                setUIShamanName = {
                    type = "function",
                    description = "Set text shaman name.", 
                    args = "text: string" 
                },
                playerVictory = {
                    type = "function",
                    description = "Gives the victory to a player.", 
                    args = "playerName: string" 
                },
                bindKeyboard = {
                    type = "function",
                    description = "Listens to the player's keyboard events.", 
                    args = "playerName: string, keyCode: int, down: bool, yes: bool" 
                },
                disableAutoShaman = {
                    type = "function",
                    description = "yes: bool", 
                    args = "Deactivates the automatic selection of Shaman." 
                },
            }
        },
        enum = {
            type = "lib",
            description = "Enums",
            childs = {
                emote = {
                    type = "lib",
                    description = "Emotes",
                    childs = {
                        sit = {
                            type = "value",
                        },                                     
                        facepaw = {
                            type = "value",
                        },
                        laugh = {
                            type = "value",
                        },
                        sleep = {
                            type = "value",
                        },
                        angry = {
                            type = "value",
                        },
                        confetti= {
                            type = "value",
                        },
                        clap = {
                            type = "value",
                        },
                        cry = {
                            type = "value",
                        },
                        kiss = {
                            type = "value",
                        },
                        dance = {
                            type = "value",
                        },                      
                    }
                },
                shamanObject = {
                    type = "lib",
                    description = "Shaman objects",
                    childs = {
                        bomb  = {
                            type = "value",
                        },
                        arrow = {
                            type = "value",
                        },
                        trampoline = {
                            type = "value",
                        },
                        balloon  = {
                            type = "value",
                        },
                        box = {
                            type = "value",
                        },
                        rune  = {
                            type = "value",
                        },
                        snowBall  = {
                            type = "value",
                        },
                        board = {
                            type = "value",
                        },
                        littleBox = {
                            type = "value",
                        },
                        cannon  = {
                            type = "value",
                        },
                        ball = {
                            type = "value",
                        },
                        anvil  = {
                            type = "value",
                        },
                        iceCube  = {
                            type = "value",
                        },
                        littleBoard  = {
                            type = "value",
                        },
                                
                    }
                }
                
                
            }
        },
        get = {
            type = "lib",
            childs = {
                misc = {
                    type = "lib",
                    childs = {                                               
                        bouboumVersion = {
                            type = "value",
                        },
                        transformiceVersion = {
                            type = "value",
                        },
                        apiVersion = {
                            type = "value",
                        }
                    }           
                },
                room = {
                    type = "lib",
                    childs = {
                        community = {
                            type = "value",
                        },
                        currentMap = {
                            type = "value",
                        },
                        maxPlayers = {
                            type = "value",
                        },
                        objectList = {
                            type = "value",
                        },
                        name = {
                            type = "value",
                        },
                        playerList = {
                            type = "lib",
                            childs = {
                                isJumping = {
                                    type = "value",
                                },
                                title = {
                                    type = "value",
                                },
                                y = {
                                    type = "value",
                                },
                                x = {
                                    type = "value",
                                },
                                isDead = {
                                    type = "value",
                                },
                                look = {
                                    type = "value",
                                },
                                isShaman = {
                                    type = "value",
                                },
                                vx = {
                                    type = "value",
                                },
                                score = {
                                    type = "value",
                                },
                                inHardMode = {
                                    type = "value",
                                },
                                vy = {
                                    type = "value",
                                },
                                movingRight = {
                                    type = "value",
                                },
                                hasCheese = {
                                    type = "value",
                                },
                                registrationDate = {
                                    type = "value",
                                },
                                playerName = {
                                    type = "value",
                                },
                                movingLeft = {
                                    type = "value",
                                },
                                isFacingRight = {
                                    type = "value",
                                },
                                isVampire = {
                                    type = "value",
                                },
                                community = {
                                    type = "value",
                                },
                            }
                        }
                    }           
                },
            }           
        }
    }
  },
  
    ui =  {
        type = "lib",
        description = "Transformice library",
        childs = {
            updateTextArea = {
                 type = "function",
                 description = "Update a text area. If targetPlayer is NIL, it sends it to all players.",
                 args = "id: int, text: string, targetPlayer: string"
            },
            removeTextArea = {
                 type = "function",
                 description = "Remove a text area. If targetPlayer is NIL, it sends it to all players.",
                 args = "targetPlayer: string, id: int"
            },
            addPopup = {
                 type = "function",
                 description = "Add a popup. Popup type : 0 (simple), 1 (yes or no), 2 (player input). If targetPlayer is NIL, it sends it to all players.",
                 args = "id: int, type: int, text: string, targetPlayer: string, x: int, y: int, width: int, fixedPos: bool"
            },
            addTextArea = {
                 type = "function",
                 description = "Add a text area. If targetPlayer is NIL, it sends it to all players.",
                 args = "id: int, text: string, tagetPlayer: string, x: int, y: int, width: int, height: int, backgroundColor: int, borderColor: int, backgroundAlpha: float, fixedPos: bool"
            }
        }
    },
    system = {
        type = "lib",
        description = "Transformice library",
        childs = {
            disableChatCommandDisplay = {
                 type = "function",
                 description = "Deactivates the display of a command in the chat window (100 max).",
                 args = "command: string, yes: bool"
            },
            newTimer = {
                 type = "function",
                 description = "Create a new timer. Return the new timer id. Use system.removeTimer(timerId) to stop a timer.",
                 args = "callback: function, time: int, loop: bool, arg1: object, arg2: object, arg3: object, arg4: object"
            },
            savePlayerData = {
                 type = "function",
                 description = "Saves the player's data for this module.",
                 args = "playerName: string, data: string"
            },
            bindMouse = {
                 type = "function",
                 description = "Listens to the player's mouse events.",
                 args = "playerName: string, yes: bool"
            },
            giveEventGift = {
                 type = "function",
                 description = "???",
                 args = "playerName: string, amount: int"
            },
            exit = {
                 type = "function",
                 description = "Deactivates the lua script running.",
            },
            removeTimer = {
                 type = "function",
                 description = "Remove a timer.",
                 args = "timerId: int"
            },
            loadPlayerData = {
                 type = "function",
                 description = "Loads the player's data for this module. Calls eventPlayerData when it's done. WARNING : eventPlayerData return # if player's data are not loaded yet server side.",
                 args = "playerName: string"
            },
            loadFile = {
                 type = "function",
                 description = "Load a file. Call limited to once per 10 minutes. Returns true if loading has started.",
                 args = "fileNumber: int"
            },
            saveFile = {
                 type = "function",
                 description = "Save a file. Call limited to once per 10 minutes. Returns true if saving has started.",
                 args = "data: string, fileNumber: int"
            },
        }    
    }
}
