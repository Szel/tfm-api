# Transformice Lua Api definitions for ZeroBrane Studio  #
[ZeroBrane studio](http://studio.zerobrane.com/) is "Lua [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) with code completion, syntax highlighting, live coding, code analyzer, and debugging support".

This script enables code completion for Transformice Lua API.

## Instalation ##
1. Copy `tfmApi.lua` file to `%ZeroBrane installation folder%/api/lua/`
2. Open `%ZeroBrane installation folder%/interpreters/luabase.lua` and replace `api = {"baselib"},` line with `api = {"baselib", "tfmApi"},`

More info: [http://studio.zerobrane.com/doc-api-auto-complete](http://studio.zerobrane.com/doc-api-auto-complete)
## Contact ##

[Create issue](https://bitbucket.org/Szel/tfm-api/issues)

Post on forum

[Private message on forum](http://atelier801.com/new-dialog?ad=Szel)
